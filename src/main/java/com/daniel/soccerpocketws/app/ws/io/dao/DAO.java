/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.io.dao;

import com.daniel.soccerpocketws.app.ws.shared.dto.UserDTO;

/**
 *
 * @author Daniel
 */
public interface DAO {
    public void openConnection();
    public UserDTO getUserByUserName(String name);
    public UserDTO saveUser(UserDTO user);
    public void closeConnection();
    public void deleteUser(UserDTO userDto);
    public void updateUser(UserDTO userDetails);

    public UserDTO getUser(String id);
}
