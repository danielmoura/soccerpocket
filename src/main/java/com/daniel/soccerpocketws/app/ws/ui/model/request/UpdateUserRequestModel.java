/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.ui.model.request;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Daniel
 */
public class UpdateUserRequestModel {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String email;
}
