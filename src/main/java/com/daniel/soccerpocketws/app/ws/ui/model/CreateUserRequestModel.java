/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.ui.model;

import javax.xml.bind.annotation.XmlRootElement;
import lombok.Setter;
import lombok.Getter;

/**
 *
 * @author Daniel
 */
@XmlRootElement
public class CreateUserRequestModel {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String password;
    
}
