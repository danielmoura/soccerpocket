/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.service;

import com.daniel.soccerpocketws.app.ws.shared.dto.UserDTO;

/**
 *
 * @author Daniel
 */
public interface UserService {
    UserDTO createUser(UserDTO user);
    UserDTO getUser(String id);
    UserDTO getUserByEmail(String userName);
    void updateUserDetails(UserDTO userDetails);
    void deleteUser(UserDTO userDto);
}
