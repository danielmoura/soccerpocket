/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.ui.model.request;

import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Daniel
 */
@XmlRootElement
public class LoginCredentials {
    @Getter
    @Setter
    private String userName;
    @Getter
    @Setter
    private String userPassword;
    
}
