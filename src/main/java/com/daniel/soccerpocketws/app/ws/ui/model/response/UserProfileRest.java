/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.ui.model.response;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Daniel
 */
public class UserProfileRest {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String password;
    @Getter
    @Setter
    private String userId;
    @Getter
    @Setter
    private String href;
}
