/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.io.dao.implementation;


import com.daniel.soccerpocketws.app.ws.io.dao.DAO;
import com.daniel.soccerpocketws.app.ws.io.entity.User;
import com.daniel.soccerpocketws.app.ws.shared.dto.UserDTO;
import com.daniel.soccerpocketws.app.ws.utils.HibernateUtils;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Daniel
 */
public class MySQLDAO implements DAO{

    
    Session session;

    @Override
    public void openConnection() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        session = sessionFactory.openSession();
    }

    @Override
    public UserDTO getUserByUserName(String userName) {

        UserDTO userDto = null;

        CriteriaBuilder cb = session.getCriteriaBuilder();

        //Create Criteria against a particular persistent class
        CriteriaQuery<User> criteria = cb.createQuery(User.class);

        //Query roots always reference entitie
        Root<User> profileRoot = criteria.from(User.class);
        criteria.select(profileRoot);
        criteria.where(cb.equal(profileRoot.get("email"), userName));

        // Fetch single result
        Query<User> query = session.createQuery(criteria);
        List<User> resultList = query.getResultList();
        if (resultList != null && resultList.size() > 0) {
            User user = resultList.get(0);
            userDto = new UserDTO();
            BeanUtils.copyProperties(user, userDto);
        }

        return userDto;
    }
    
    
    public UserDTO getUser(String id) {
         CriteriaBuilder cb = session.getCriteriaBuilder();

        //Create Criteria against a particular persistent class
        CriteriaQuery<User> criteria = cb.createQuery(User.class);

        //Query roots always reference entitie
        Root<User> profileRoot = criteria.from(User.class);
        criteria.select(profileRoot);
        criteria.where(cb.equal(profileRoot.get("userId"), id));

        // Fetch single result
        User user = session.createQuery(criteria).getSingleResult();
        
        UserDTO userDto = new UserDTO();
        BeanUtils.copyProperties(user, userDto);
        
        return userDto;
    }
    
    @Override
    public UserDTO saveUser(UserDTO userDTO)
    {
        UserDTO returnValue = null;
        User user = new User();
        BeanUtils.copyProperties(userDTO, user);
        
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
        
        returnValue = new UserDTO();
        BeanUtils.copyProperties(user, returnValue);
        
        return returnValue;
    }

    @Override
    public void closeConnection() {
        if (session != null) {
            session.close();
        }
    }

    public void updateUser(UserDTO userProfile) {
     User user = new User();
     BeanUtils.copyProperties(userProfile, user);
     
     session.beginTransaction();
     session.update(user);
     session.getTransaction().commit();
    }
    
    @Override
    public void deleteUser(UserDTO userPofile) {
        User user = new User();
        BeanUtils.copyProperties(userPofile, user);
        
        session.beginTransaction();
        session.delete(user);
        session.getTransaction().commit();
    }

//    @Override
//    public List<UserDTO> getUsers(int start, int limit) {
//
//        CriteriaBuilder cb = session.getCriteriaBuilder();
//
//        //Create Criteria against a particular persistent class
//        CriteriaQuery<User> criteria = cb.createQuery(User.class);
//
//        //Query roots always reference entities
//        Root<User> userRoot = criteria.from(User.class);
//        criteria.select(userRoot);
//
//        // Fetch results from start to a number of "limit"
//        List<User> searchResults = session.createQuery(criteria).
//                setFirstResult(start).
//                setMaxResults(limit).
//                getResultList();
// 
//        List<UserDTO> returnValue = new ArrayList<UserDTO>();
//        for (User user : searchResults) {
//            UserDTO userDto = new UserDTO();
//            BeanUtils.copyProperties(user, userDto);
//            returnValue.add(userDto);
//        }
//
//        return returnValue;
//    }
//
//
//    @Override
//    public UserDTO getUserByEmailToken(String token) {
//       CriteriaBuilder cb = session.getCriteriaBuilder();
//
//        //Create Criteria against a particular persistent class
//        CriteriaQuery<User> criteria = cb.createQuery(User.class);
//
//        //Query roots always reference entitie
//        Root<User> profileRoot = criteria.from(User.class);
//        criteria.select(profileRoot);
//        criteria.where(cb.equal(profileRoot.get("emailVerificationToken"), token));
//
//        // Fetch single result
//        User user = session.createQuery(criteria).getSingleResult();
//        
//        UserDTO userDto = new UserDTO();
//        BeanUtils.copyProperties(user, userDto);
//        
//        return userDto;
//    }

    
}
