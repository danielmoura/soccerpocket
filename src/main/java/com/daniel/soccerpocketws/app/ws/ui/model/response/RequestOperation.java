/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.ui.model.response;

/**
 * 
 * @author Daniel
 */
public enum RequestOperation {
    CREATE, UPDATE, DELETE
}
