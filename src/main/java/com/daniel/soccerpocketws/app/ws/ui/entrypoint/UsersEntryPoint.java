/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.ui.entrypoint;

import com.daniel.soccerpocketws.app.ws.service.UserService;
import com.daniel.soccerpocketws.app.ws.service.implementation.UsersServiceImplementation;
import com.daniel.soccerpocketws.app.ws.shared.dto.UserDTO;
import com.daniel.soccerpocketws.app.ws.ui.model.CreateUserRequestModel;
import com.daniel.soccerpocketws.app.ws.ui.model.request.UpdateUserRequestModel;
import com.daniel.soccerpocketws.app.ws.ui.model.response.DeleteUserProfileResponseModel;
import com.daniel.soccerpocketws.app.ws.ui.model.response.RequestOperation;
import com.daniel.soccerpocketws.app.ws.ui.model.response.UserProfileRest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.BeanUtils;
import sun.security.provider.certpath.OCSPResponse.ResponseStatus;

/**
 *
 * @author Daniel
 */
@Path("/users")
public class UsersEntryPoint {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public UserProfileRest createUser(CreateUserRequestModel requestModel) {
        UserProfileRest userProfileRest = new UserProfileRest();
        //Prepare to use UserDTO
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(requestModel, userDTO);
        //Create new user
        UserService userService = new UsersServiceImplementation();
        UserDTO createdUserProfile = userService.createUser(userDTO);
        //Prepare to response
        BeanUtils.copyProperties(createdUserProfile, userProfileRest);
        return userProfileRest;
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public UserProfileRest getUserProfile(@PathParam("id") String id) {
        UserProfileRest returnValue = null;

        UserService userService = new UsersServiceImplementation();
        UserDTO userProfile = userService.getUser(id);

        returnValue = new UserProfileRest();
        BeanUtils.copyProperties(userProfile, returnValue);

        return returnValue;
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public UserProfileRest updateUserDetails(@PathParam("id") String id, UpdateUserRequestModel userDetails) {

        UserService userService = new UsersServiceImplementation();
        UserDTO storedUserDetails = userService.getUser(id);

        // Set only those fields you would like to be updated with this request
        if (userDetails.getName() != null && !userDetails.getName().isEmpty()) {
            storedUserDetails.setName(userDetails.getName());
        }
        if (userDetails.getEmail()!= null && !userDetails.getEmail().isEmpty()) {
            storedUserDetails.setEmail(userDetails.getEmail());
        }

        // Update User Details
        userService.updateUserDetails(storedUserDetails);

        // Prepare return value 
        UserProfileRest returnValue = new UserProfileRest();
        BeanUtils.copyProperties(storedUserDetails, returnValue);

        return returnValue;
    }

    @DELETE
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public DeleteUserProfileResponseModel deleteUserProfile(@PathParam("id") String id) {
        DeleteUserProfileResponseModel returnValue = new DeleteUserProfileResponseModel();
        returnValue.setRequestOperation(RequestOperation.DELETE);

        UserService userService = new UsersServiceImplementation();
        UserDTO storedUserDetails = userService.getUser(id);

        userService.deleteUser(storedUserDetails);

        returnValue.setResponseStatus(ResponseStatus.SUCCESSFUL);

        return returnValue;
    }
}
