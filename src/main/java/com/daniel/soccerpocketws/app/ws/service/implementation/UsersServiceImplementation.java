/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.service.implementation;

import com.daniel.soccerpocketws.app.ws.exception.CouldNotCreateRecordException;
import com.daniel.soccerpocketws.app.ws.exception.NoRecordFoundException;
import com.daniel.soccerpocketws.app.ws.io.dao.DAO;
import com.daniel.soccerpocketws.app.ws.io.dao.implementation.MySQLDAO;
import com.daniel.soccerpocketws.app.ws.service.UserService;
import com.daniel.soccerpocketws.app.ws.shared.dto.UserDTO;
import com.daniel.soccerpocketws.app.ws.ui.model.response.ErrorMessages;
import com.daniel.soccerpocketws.app.ws.utils.UserProfileUtils;

/**
 *
 * @author Daniel
 */
public class UsersServiceImplementation implements  UserService{
    DAO database;

    public UsersServiceImplementation() {
        this.database = new MySQLDAO();
    }
    @Override
    public UserDTO createUser(UserDTO userDTO) {
        //Validate fields
        UserProfileUtils.validateRequiredFields(userDTO);
        
        //Check if exists
        UserDTO existingUser = this.getUserByEmail(userDTO.getEmail());
        if(existingUser!=null){
            throw new CouldNotCreateRecordException(ErrorMessages.RECORD_ALREADY_EXISTS.name());
        }
        //Generate public secure user id
        String userId = UserProfileUtils.generateUserId(20);
        userDTO.setUserId(userId);
        //Salt and pass generation
        String salt = UserProfileUtils.getSalt(20);
        String securePass = UserProfileUtils.generateSecurePassword(userDTO.getEmail(), salt);
        userDTO.setEncryptedPassword(securePass);
        userDTO.setSalt(salt);
        // Record in database
        UserDTO udto = this.saveUser(userDTO);
        
        return udto;
    }

    @Override
    public UserDTO getUserByEmail(String email) {
        UserDTO userDto = null;
        if (email == null || email.isEmpty()) {
            return userDto;
        }

        // Connect to database 
        try {
            this.database.openConnection();
            userDto = this.database.getUserByUserName(email);
        } finally {
            this.database.closeConnection();
        }

        return userDto;
    }
    
    private UserDTO saveUser(UserDTO user) {
        UserDTO returnValue = null;
        // Connect to database 
        try {
            this.database.openConnection();
            returnValue = this.database.saveUser(user);
        } finally {
            this.database.closeConnection();
        }

        return returnValue;
    }

    @Override
    public UserDTO getUser(String id) {
        UserDTO returnValue = null;
        try {
            this.database.openConnection();
            returnValue = this.database.getUser(id);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new NoRecordFoundException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        } finally {
            this.database.closeConnection();
        }
        return returnValue;
    }

    @Override
    public void updateUserDetails(UserDTO userDetails) {
        try {
            // Connect to database 
            this.database.openConnection();
            // Update User Details
            this.database.updateUser(userDetails);

        } catch (Exception ex) {
            throw new CouldNotCreateRecordException(ex.getMessage());
        } finally {
            this.database.closeConnection();
        }
    }

    @Override
    public void deleteUser(UserDTO userDto) {
        try {
            this.database.openConnection();
            this.database.deleteUser(userDto);
        } catch (Exception ex) {
            throw new CouldNotCreateRecordException(ex.getMessage());
        } finally {
            this.database.closeConnection();
        }

        // Verify that user is deleted
        try {
            userDto = getUser(userDto.getUserId());
        } catch (NoRecordFoundException ex) {
            userDto = null;
        }

        if (userDto != null) {
            throw new CouldNotCreateRecordException(
                    ErrorMessages.COULD_NOT_DELETE_RECORD.getErrorMessage());
        }
    }
    
}
