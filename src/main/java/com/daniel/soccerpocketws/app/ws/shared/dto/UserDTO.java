/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.shared.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Daniel
 */
public class UserDTO implements Serializable {
    private static final long  serialVersionUID = 1L;
    @Getter
    @Setter
    private long id;
    @Getter
    @Setter
    private String userId;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String password;
    @Getter
    @Setter
    private String encryptedPassword;
    @Getter
    @Setter
    private String salt;
    @Getter
    @Setter
    private String token;
}
