/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daniel.soccerpocketws.app.ws.exception;

/**
 *
 * @author Daniel
 */
public class NoRecordFoundException extends RuntimeException{
    
    private static final long serialVersionUID = 5776681206288518465L;
    
    public NoRecordFoundException(String message)
    {
        super(message);
    }
    
}
